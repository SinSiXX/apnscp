<?php
    require_once dirname(__DIR__, 2) . '/TestFramework.php';

    class VersionComparisonsTest extends TestFramework
    {
    	const IR_VERSION = '1.8.2.beta2';
    	const REG_VERSION = '1.8.2';
    	const VERSIONS = ['1.8.1', '1.8.2.beta1', '1.8.2.beta2', '1.8.2.beta3', '1.8.2', '1.8.3.beta1', '1.8.3', '1.8.4'];

        public function testBasic()
        {
        	$this->assertEquals('1', \Opcenter\Versioning::asMajor(self::IR_VERSION), 'Version as major branch');
        	$this->assertEquals('1.8', \Opcenter\Versioning::asMinor(self::IR_VERSION), 'Version as minor');
        	$this->assertEquals('1.8.2', \Opcenter\Versioning::asPatch(self::IR_VERSION), 'Version as patch');
            return true;
        }

        public function testStripExperimental() {
        	$this->assertSame(self::REG_VERSION, \Opcenter\Versioning::asPatch(self::IR_VERSION));
		}

        public function testIrregularVersionNext()
		{
			$this->assertNotEquals(self::IR_VERSION, \Opcenter\Versioning::nextVersion(self::VERSIONS, self::IR_VERSION));
			$this->assertTrue(version_compare(\Opcenter\Versioning::nextVersion(self::VERSIONS, self::IR_VERSION), \Opcenter\Versioning::asPatch(self::IR_VERSION), '<'));
			$this->assertTrue(version_compare(self::IR_VERSION, \Opcenter\Versioning::nextVersion(self::VERSIONS, self::IR_VERSION), '<'));
		}

		public function testUnparseableVersion()
		{
			$oldex = \Error_Reporter::exception_upgrade(Error_Reporter::E_FATAL);
			try {
				\Opcenter\Versioning::asPatch('1.8beta3');
			} catch (\apnscpException $e) {
				$this->assertContains("unparseable version", $e->getMessage());
				return true;
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}
			$this->assertFalse(true, "Erroneous version could be parsed");
		}

		public function testMaximalNotFound()
		{
			$tmp = '7.3.0';
			$this->assertEquals($tmp, \Opcenter\Versioning::nextSemanticVersion(
				$tmp,
				['7.4.0', '7.4.1', '7.4.2'],
				'minor'
			), 'No satisfied version found');
		}

		public function testMaximalFound()
		{
			$target = '7.4.2';
			$this->assertEquals($target, \Opcenter\Versioning::nextSemanticVersion(
				'7.3.0',
				['7.4.0', '7.4.1', $target, '8.1.1'],
				'major'
			), 'Version satisified by minor bump');
		}

		public function testCanonVersion()
		{
			$this->assertTrue(\Opcenter\Versioning::compare('10', '10.2', '='));
			$this->assertTrue(\Opcenter\Versioning::compare('10', '10.2.9', '>='));
			$this->assertFalse(\Opcenter\Versioning::compare('9.6', '10', '='));
			$this->assertFalse(\Opcenter\Versioning::compare('9.6', '9', '<'));
		}

		public function testSatisfy()
		{
			$versions = [
				'1'     => '1.2',
				'2.5'   => '1.3',
				'2.6.2' => '1.4'
			];

			$this->assertEquals(
				\Opcenter\Versioning::satisfy('1', $versions),
				'1.2'
			);

			$this->assertNull(
				\Opcenter\Versioning::satisfy('0', $versions),
			);

			$this->assertEquals(
				\Opcenter\Versioning::satisfy('2.5', $versions),
				'1.3'
			);

			$this->assertEquals(
				\Opcenter\Versioning::satisfy('2.6.3', $versions),
				'1.4'
			);
		}
	}

