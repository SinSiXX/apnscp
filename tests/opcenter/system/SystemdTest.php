<?php

	use Opcenter\Http\Apache;

	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class SystemdTest extends TestFramework
	{
		const EXCEPTION_LEVEL = Error_Reporter::E_ERROR;

		public function testApacheRestart()
		{
			$start = $this->getStartTime();
			sleep(2);
			$this->assertTrue(Apache::run('restart'), 'Apache restart succeeded');
			sleep(5);
			$this->assertNotEquals($start, $this->getStartTime(), 'Apache service restarted');
			return true;
		}

		protected function getStartTime(): int {
			$status = Apache::status();
			return (int)array_get($status, 'ExecMainStartTimestampMonotonic', 0);
		}
	}

