<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class PassengerTest extends TestFramework
	{
		protected $account;

		public function __construct(?string $name = null, array $data = [], string $dataName = '')
		{
			parent::__construct($name, $data, $dataName);

		}


		public function setUp()
		{
			$this->account = silence(function() {
				return \Opcenter\Account\Ephemeral::create();
			});
		}

		public function tearDown()
		{
			$this->account = null;
		}

		public function testEnvironmentFormatting()
		{
			if (!SSH_USER_DAEMONS) {
				return $this->markTestSkipped('[ssh] => user_daemons disabled');
			}
			$this->assertTrue(\apnscpSession::init()->exists($this->account->getContext()->id));
			$passenger = \Module\Support\Webapps\Passenger::instantiateContexted($this->account->getContext(), ['/var/www/html', 'ruby']);
			$passenger->setEngine('nginx');
			$passenger->setEnvironment([
				'RUBY_GC_HEAP_GROWTH_MAX_SLOTS'       => 40000,
				'RUBY_GC_HEAP_INIT_SLOTS'             => 400000,
				'RUBY_GC_HEAP_OLDOBJECT_LIMIT_FACTOR' => 1.5
			]);
			$json = json_decode($passenger->getExecutableConfiguration(), true);
			$this->assertInternalType('array', $json);
			$this->assertArrayHasKey('envvars', $json);
			$this->assertArrayHasKey('RUBY_GC_HEAP_INIT_SLOTS', $json['envvars']);
			return true;
		}

		public function testLoadPassengerFile()
		{
			if (!SSH_USER_DAEMONS) {
				return $this->markTestSkipped('[ssh] => user_daemons disabled');
			}
			$passenger = \Module\Support\Webapps\Passenger::instantiateContexted($this->account->getContext(),
				['/var/www/html', 'ruby']);
			$passenger->setEngine('nginx');
			$passenger->setEnvironment([
				'RUBY_GC_HEAP_GROWTH_MAX_SLOTS'       => 40000,
				'RUBY_GC_HEAP_INIT_SLOTS'             => 400000,
				'RUBY_GC_HEAP_OLDOBJECT_LIMIT_FACTOR' => 1.5
			]);
			$this->assertTrue($passenger->saveExecutableConfiguration(), 'Passengerfile.json saved');
			$this->assertContains('RUBY_GC_HEAP_INIT', (string)$passenger, 'Passenger environment variables');
			$passenger2 = \Module\Support\Webapps\Passenger::load('/var/www/html', $this->account->getContext());
			$this->assertSame(
				var_export($passenger2->getExecutableConfiguration(), true), var_export($passenger->getExecutableConfiguration(), true),
				'Passengerfile.json loading'
			);

		}
	}

