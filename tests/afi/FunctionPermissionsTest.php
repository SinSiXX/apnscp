<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class FunctionPermissionsTest extends TestFramework
    {
        const BAD_SECONDARY_CMD = 'site_get_bandwidth_rollover';
        protected $filename;
        public function testSecondaryUserPermission()
        {
            \Error_Reporter::set_verbose(0);
            $auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
            $afi1 = apnscpFunctionInterceptor::factory($auth1);
            $mockuser = 'demo-user-' . random_int(1000, 9999);
            $this->assertTrue($afi1->user_add_user($mockuser, \Opcenter\Auth\Password::generate()));
            $userafi = \apnscpFunctionInterceptor::factory(
                \Auth::context($mockuser, array_get(Definitions::get(), 'auth.site.domain'))
            );
            $this->assertFalse($userafi->{self::BAD_SECONDARY_CMD}());
            $this->assertTrue($afi1->user_delete_user($mockuser));
            $this->assertNotFalse($afi1->{self::BAD_SECONDARY_CMD}());
        }
    }

