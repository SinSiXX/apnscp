<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class IpRestrictionTest extends TestFramework
	{
		public function testSelfSignedInstallation()
		{
			$ephemeral = \Opcenter\Account\Ephemeral::create();
			$ip = long2ip(random_int(1, 2**32-1));
			$afi = $ephemeral->getApnscpFunctionInterceptor();
			$this->assertArrayNotHasKey($ip, $afi->auth_get_ip_restrictions());
			$this->assertTrue($afi->auth_restrict_ip($ip));
			$this->assertArrayHasKey($ip, $afi->auth_get_ip_restrictions());
			$this->assertTrue($afi->auth_remove_ip_restriction($ip));
			$this->assertArrayNotHasKey($ip, $afi->auth_get_ip_restrictions());
		}
	}