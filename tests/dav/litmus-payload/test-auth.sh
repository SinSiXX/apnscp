#!/bin/sh
# debug@debug.com/debug
AUTH=ZGVidWdAZGVidWcuY29tOmRlYnVn
# test99@debug.com/test
#AUTH=dGVzdDk5QGRlYnVnLmNvbTp0ZXN0
METHOD=PROPFIND
RESOURCE="/var/www/html"
if [ ! -z $2 ] ; then
	METHOD=$1
	RESOURCE=$2
fi
( echo "$METHOD $RESOURCE HTTP/1.1" 
  echo "Host: www.debug.com" 
  echo "Authorization: Basic $AUTH" 
  echo "") | nc 192.168.0.10 2077
