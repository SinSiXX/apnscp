<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	class Cache_Base
	{
		const LVLOCAL = 4;
		const LVACCOUNT = 2;
		const LVGLOBAL = 1;
		const CACHE_PROXY = 'Cache_Mproxy';

		// session id in contextables
		protected static $context;
		protected static $key = '';
		protected static $auth;
		private static $instance = array();

		private function __construct()
		{
		}

		/**
		 * @param \Auth_Info_User $context
		 * @return \Redis
		 */
		public static function spawn(\Auth_Info_User $context = null): \Redis
		{
			static::reset($context);

			if (!isset(self::$instance[static::class])) {
				self::$instance[static::class] = self::_init();
			}

			return self::$instance[static::class];
		}

		/**
		 * Disconnect all Cache instances
		 */
		public static function disconnect() {
			foreach (self::$instance as $conn) {
				$conn->disconnect();
			}
			self::$instance = [];
		}

		public static function reset(\Auth_Info_User $context = null)
		{
			if ($context) {
				static::$auth = $context;
			}
			static::reset_prefix();
			if (!isset(self::$instance[static::class])) {
				return false;
			}
			self::$instance[static::class]->
			setOption(\Redis::OPT_PREFIX, static::$key);
		}

		public static function reset_prefix()
		{
			$key = '';
			static::set_key($key);
		}

		public static function set_key($key)
		{
			static::$key = $key;
		}

		private static function _init()
		{
			$c = call_user_func([static::CACHE_PROXY, 'init']);
			static::reset_prefix();
			$key = static::$key;
			$c->setOption(\Redis::OPT_PREFIX, $key);

			return $c;
		}

		public static function role_prefix(\Auth_Info_User $ctx = null)
		{
			$user = $ctx ?? static::__get_auth();
			if (!$user) {
				return false;
			}
			$key = 'a';
			if ($user->level & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				$key = 'u-' . $user->site_id;
			} else if ($user->level & (PRIVILEGE_RESELLER)) {
				$key = 'r';
			}

			return $key;
		}

		private static function __get_auth()
		{
			return static::$auth;
		}

		public static function role_user()
		{
			$user = static::__get_auth();
			if (!$user) {
				return false;
			}

			// virtual users, such as reseller?
			return $user->user_id ?? $user->username;
		}

		public static function role_site_id()
		{
			$user = static::__get_auth();
			if (!$user) {
				return false;
			}
			if ($user->level & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				return $user->site_id;
			}

			return '';

		}

		public static function flush_cache()
		{
			return self::$instance[static::class]->flush();
		}
	}