<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Event;

	use Contracts;
	use Event\Contracts\Publisher;
	use Event\Contracts\Subscriber;

	class Manager
	{
		/**
		 * @var array[] callback events
		 */
		protected $listeners = [];

		protected $eventQueue = [];

		public function __construct()
		{

		}

		/**
		 * Register an event to run on a fired event
		 *
		 * @param array|string $event event
		 * @param Subscriber   $subscriber
		 * @param int          $opts  event options
		 */
		public function register($event, Subscriber $subscriber, int $opts = 0): void
		{
			if (\is_array($event)) {
				$event = join('.', $event);
			}

			if (\is_array($subscriber)) {
				if ($opts & Cardinal::OPT_SINGLE) {
					warn("multiple events registered for `%s', but single event option specified", $event);
				}
				$this->attachObserverArray($event, $subscriber, $opts);

				return;
			}

			$queue = array_get($this->listeners, $event, []);
			$arr = ['cb' => $subscriber, 'options' => $opts];
			if ($opts & Cardinal::OPT_SINGLE) {
				$queue = [$arr];
			} else if ($opts & Cardinal::OPT_PREPEND) {
				array_unshift($queue, $arr);
			} else {
				$queue[] = $arr;
			}
			array_set($this->listeners, $event, $queue);
		}

		/**
		 * Used internally to attach an array of ObserverInterface implementations
		 *
		 * @param string $event
		 * @param array  $observers
		 * @param int    $opts event options
		 * @return $this
		 */
		private function attachObserverArray(string $event, array $observers, int $opts = 0)
		{
			foreach ($observers as $observer) {
				$this->register($event, $observer, $opts);
			}

			return $this;
		}

		/**
		 * Remove a callback from the event queue
		 *
		 * @param string              $event
		 * @param array|Observer|null $observer
		 */
		public function remove(string $event, $observer = null)
		{
			if (\is_array($observer)) {
				return $this->detachObserverArray($event, $observer);
			} else if (null === $observer) {
				return array_forget($this->listeners, $event);
			}
			$remove = array_where(array_get($this->listeners, $event, []), static function ($idx, $cb) use ($observer) {
				return $cb['cb'] === $observer;
			});

			array_forget($this->listeners, $event, array_keys($remove));

			return;
		}

		/**
		 * Used internally to detach an array of observers
		 *
		 * @param string $event
		 * @param array  $observers
		 * @return void
		 */
		private function detachObserverArray(string $event, array $observers): void
		{
			foreach ($observers as $observer) {
				$this->remove($event, $observer);
			}

			return;
		}

		/**
		 * Fire a registered event
		 *
		 * Specify *.<type> to fire an event on all registered listeners for an event
		 *
		 * @param array|string $event event, supports dot notation
		 * @param Publisher    $caller
		 * @return bool|null callback status or null if no callbacks fired
		 *
		 */
		public function fire($event, Publisher $caller): ?bool
		{
			if (\is_array($event)) {
				// allow firing to global event handlers
				for ($i = \count($event) - 1; $i > 0; $i--) {
					$this->fire($event[$i], $caller);
				}
				$event = join('.', $event);
			}
			// accept global namespaced event firing
			if (0 === strncmp($event, '*.', 2)) {
				$wc = substr($event, 2);
				$tmp = array_map(static function ($events) use($wc) {
					return $events[$wc] ?? [];
				}, $this->listeners);
				$callbacks = [];
				foreach (array_filter($tmp) as $cbs) {
					foreach($cbs as $cb) {
						$callbacks[] = $cb;
					}
				}
			} else {
				$callbacks = array_get($this->listeners, $event, []);
			}

			if (!$callbacks) {
				return null;
			}
			$ret = true;
			foreach ($callbacks as $cb) {
				try {
					$tmp = $cb['cb']->update($event, $caller);
				} catch (\Throwable $e) {
					$msg = $e->getMessage();
					$args = [];
					if (is_debug()) {
						$msg .= "\nBT: %s";
						$args[] = $e->getTraceAsString();
					}
					$tmp = error($msg, $args);
				}
				if ($tmp === false) {
					if ($cb['options'] & Cardinal::OPT_ATOMIC) {
						if (is_debug()) {
							pause("Cardinal event `%s' failed in `%s' (caller: `%s')",
								$event,
								\get_class($cb['cb']),
								\Error_Reporter::get_caller(2)
							);
							var_dump($cb['cb']);
						}

						return false;
					}
					$ret &= $tmp;
				} else {
					// null and true are considered "OK"
					$ret &= true;
				}
			}

			return (bool)$ret;
		}
	}