<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * @package Compression
	 * Provides zip compression/decompression routines in the file manager
	 */
	class Zip_Filter extends Archive_Base
	{
		public static function extract_files($archive, $dest, array $file = null, ?array $opts = array())
		{
			$zip = new ZipArchive();
			if (!$zip->open($archive)) {
				return error("unable to open archive `%s': ",
					$archive,
					$zip->getStatusString()
				);
			}
			$zip->extractTo($dest, $file);
			$zip->close();

			return true;
		}

		public static function list_files($archive, ?array $opts = array())
		{
			$proc = parent::exec('/usr/bin/unzip -ll -- %s',
				$archive,
				array(1, 0));
			$proc = explode("\n", trim($proc['output']));
			$files = array();

			foreach ($proc as $line) {

				if (!preg_match(Regex::FILE_HDR_ZIP, $line, $matches)) {
					continue;
				}

				list ($null, $size, $method, $packed_size, $date, $crc, $file_name) = $matches;
				$files[] = array(
					'file_name'   => $file_name,
					'file_type'   => $file_name[strlen($file_name) - 1] == '/' ? 'dir' : 'file',
					'can_read'    => true,
					'can_write'   => true,
					'can_execute' => true,
					'size'        => !$size ? 4095 : $size,
					'packed_size' => $packed_size,
					'crc'         => $crc,
					'link'        => 0,
					'date'        => strtotime($date)
				);
			}

			return $files;

		}
	}

?>
