<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	use Opcenter\SiteConfiguration;

	/**
	 * Grant access to info/ helpers
	 */
	trait AccountInfoTrait {
		use ContextableTrait;


		/**
		 * Get all service configuration
		 *
		 * @return array
		 */
		protected function getServices(): array
		{
			return $this->getAuthContext()->conf->conf;
		}

		/**
		 * Get unsynchronized configuration
		 *
		 * @param string $service
		 * @return array|null
		 */
		protected function getNewServices(string $service = null): ?array
		{
			$s = $this->getAuthContext()->conf->new;
			if ($service) {
				return $s[$service] ?? null;
			}

			return $s;
		}

		/**
		 * Get previous configuration
		 *
		 * @param string $service
		 * @return array|null
		 */
		protected function getOldServices(string $service = null): ?array
		{
			$s = $this->getAuthContext()->conf->old;
			if ($service) {
				return $s[$service] ?? null;
			}

			return $s;
		}

		/**
		 * Get current service configuration
		 *
		 * @param string|null $svc
		 * @return array|null
		 */
		protected function getActiveServices(string $svc = null): ?array
		{
			$s = $this->getAuthContext()->conf->cur;
			if ($svc) {
				return $s[$svc] ?? null;
			}

			return $s;
		}

		/**
		 * Get service configuration
		 *
		 * Alias to getServiceValue($service, $parameter)
		 *
		 * @param string      $service
		 * @param string|null $parameter
		 * @return mixed
		 */
		protected function getConfig(string $service, string $parameter = null)
		{
			return $this->getServiceValue($service, $parameter);
		}

		/**
		 * mixed get_service_value(string, string)
		 * Get site configuration value from <FST>/info/current
		 *
		 * @param string $service   service type to look up data
		 * @param string $parameter|null name of the service type subset to return data
		 * @param mixed  $default   default value
		 * @return mixed
		 */
		protected function getServiceValue(string $service, string $parameter = null, $default = null)
		{
			if ($this->getAuthContext()->level & PRIVILEGE_ADMIN) {
				return $default;
			}
			$conf = $this->getAuthContext()->conf->cur[$service] ?? $this->getAuthContext()->conf->new[$service] ?? null;

			if (null === $conf) {
				// newer platform remaps
				$remap = array_get(SiteConfiguration::MODULE_REMAP, $service);
				if ($remap && $remap !== $service) {
					return $this->getServiceValue($remap, $parameter, $default);
				}
				return $default;
			}

			if (!$parameter) {
				return $conf;
			}

			if (!isset($conf[$parameter])) {
				return $default;
			}
			$val = $conf[$parameter];

			return $val;
		}

		/**
		 *  Journals data to <site config>/info/new/<$svc_name>
		 *  Invokes reconfigure hook when EditVirtDomain is used
		 *
		 * @param string $service
		 * @param string $parameter
		 * @param mixed $value
		 * @return Auth_Info_Account
		 */
		protected function setConfigJournal(string $service, string $parameter, $value): \Auth_Info_Account
		{
			return $this->setConfig($service, $parameter, $value, true);
		}

		/**
		 *  Writes out configuration immediately to <site config>/info/current/<$svc_name>
		 *  *Does not* invoke reconfigure hook when EditVirtDomain is used
		 *
		 * @param string $service service
		 * @param string $parameter service parameter
		 * @param mixed  $value service value
		 * @param bool   $journal write to unsynchronized journal (new/)
		 * @return Auth_Info_Account
		 */
		protected function setConfig(string $service, string $parameter, $value, bool $journal = false): \Auth_Info_Account
		{
			$conf = $this->getAuthContext()->conf;
			// hook into \Auth_Info_Account()
			$conf->change($service, array($parameter => $value), $journal);
			if ($journal) {
				$new = $conf->new;
			} else {
				$new = $conf->cur;
			}
			\DataStream::get($this->getAuthContext())->query('common_save_service_information_backend', $new, $journal);
			return $this->getAuthContext()->reset();
		}
	}