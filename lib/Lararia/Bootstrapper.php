<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	declare(strict_types=1);

	namespace Lararia;

	/**
	 * Class Bootstrapper
	 *
	 * Wrapper to include("bootstrap/app.php");
	 *
	 * @package Lararia
	 */
	class Bootstrapper
	{
		private static $app;

		/**
		 * Provide minimal amount of bootstrapping necessary to use Laravel within apnscp
		 */
		public static function minstrap(): Application
		{
			if (null !== static::$app && class_exists(Application::class, false)) {
				return static::$app;
			}
			$app = \Lararia\Bootstrapper::app();
			// @TODO clean up, move into boot()
			$kernel = $app->make(Contracts\SkinnyKernel::class);
			$kernel->handle(
				null,
				null
			);

			return static::$app = $app;
		}

		public static function app(): Application
		{
			return include(__DIR__ . '/app.php');
		}
	}