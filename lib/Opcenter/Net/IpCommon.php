<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Net;

	use IPTools\IP;
	use IPTools\Network;
	use IPTools\Range;

	class IpCommon
	{

		/**
		 * Verify if IP address has assignment
		 *
		 * @param string $ip ip address
		 * @return bool
		 */
		public static function ip_allocated(string $ip): bool
		{
			// @todo look is udp & lossy - retry query a few times to be extra certain
			for ($i = 0; $i < 5; $i++) {
				if (\Net_Gethost::gethostbyaddr_t($ip, 2000, \Dns_Module::AUTHORITATIVE_NAMESERVERS)) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Requested address family supported
		 *
		 * @return bool
		 */
		public static function enabled(): bool
		{
			return false;
		}

		/**
		 * IP in range
		 *
		 * @param string $ip
		 * @param string $range
		 * @return bool
		 */
		public static function in(string $ip, string $range): bool
		{
			try {
				return Range::parse($range)->contains(new IP($ip));
			} catch (\Exception $e) {
				return false;
			}
		}

		/**
		 * Specified IP address is reserved
		 *
		 * @param string $ip
		 * @return bool
		 */
		public static function reserved(string $ip): bool
		{
			static $seen = [];

			if (isset($seen[$ip])) {
				return $seen[$ip];
			}

			if (static::class === self::class) {
				// *sigh*
				if (false !== strpos($ip, ':')) {
					return Ip6::reserved($ip);
				}

				return Ip4::reserved($ip);
			}

			// declaring this earlier would allow $compiled to be shared between both classes
			static $compiled;

			if (!isset($compiled)) {
				$tmp = [];
				foreach (static::RESERVED_BLOCKS as $block) {
					$parsed = Network::parse($block);
					$tmp[] = [
						$parsed->getFirstIP()->toLong(),
						$parsed->getLastIP()->toLong()
					];
				}
				$compiled = $tmp;
			}

			$ret = false;
			$comp = (new IP($ip))->toLong();
			foreach ($compiled as [$min, $max]) {
				if (bccomp($comp, $min) === -1 || bccomp($comp, $max) === 1) {
					continue;
				}
				$ret = true;
				break;
			}

			return $seen[$ip] = $ret;
		}

		/**
		 * IP matches target
		 *
		 * @param string $ip
		 * @param string $match IPv4, IPv6, or CIDR range
		 * @return bool
		 */
		public static function is(string $ip, string $match): bool
		{
			if (false !== strpos($match, '/')) {
				return static::in($ip, $match);
			}

			return inet_pton($ip) === inet_pton($match);
		}

		/**
		 * Address is machine-supported valid IPv4, IPv6, or CIDR
		 *
		 * @param string $address
		 * @return bool
		 */
		public static function valid(string $address): bool
		{
			if (!$address) {
				return false;
			}

			try {
				if (false === strpos($address, '/')) {
					return false !== inet_pton($address);
				}
				Network::parse($address)->getFirstIP();
			} catch (\Exception $e) {
				return false;
			}

			return true;
		}

		/**
		 * Requested internet protocol supported
		 *
		 * @param string $address
		 * @param string $class optional class detected
		 * @return bool
		 */
		public static function supported(string $address, string &$class = null): bool
		{
			$checker = false === strpos($address, ':') ? Ip4::class : Ip6::class;
			if (null !== $class) {
				$class = static::classFromAddress($address);
			}

			return $checker::enabled();
		}

		/**
		 * Get IP class from address
		 *
		 * @param string $address
		 * @return string "ip4" or "ip6"
		 */
		public static function classFromAddress(string $address): ?string
		{
			$checker = false === strpos($address, ':') ? Ip4::class : Ip6::class;
			return strtolower(basename(str_replace('\\', '/', $checker)));
		}

		/**
		 * Get auto-detected server IP
		 *
		 * @return string
		 */
		public static function my_ip(): string
		{
			return Ip4::enabled() ? Ip4::my_ip() : Ip6::my_ip();
		}

		public static function format(string $ip): string
		{
			if (false === strpos($ip, ':')) {
				return $ip;
			}

			return $ip[0] === '[' ? $ip : "[$ip]";
		}

		/**
		 * IP address CIDR range or address
		 *
		 * @param string $addr
		 * @return string
		 */
		public static function family(string $addr): bool
		{
			if (false === strpos($addr, '/')) {
				return static::valid($addr);
			}

			try {
				$range = Range::parse($addr);
			} catch (\Exception $e) {
				return false;
			}

			if (self::class === static::class) {
				// is valid IPv4/IPv6 range
				return true;
			}

			$ip = $range->getFirstIP()->inAddr();
			if ($range->getFirstIP()->getVersion() === IP::IP_V4) {
				return static::valid($ip);
			}

			// Convert to hextet
			return static::valid(inet_ntop($ip));
		}
	}

