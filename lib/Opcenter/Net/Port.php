<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Net;

	use Opcenter\Filesystem;

	class Port
	{

		/**
		 * Get first available port for account
		 *
		 * @param \Auth_Info_User $context
		 * @return int|null
		 */
		public static function firstFree(\Auth_Info_User $context): ?int
		{
			if (!$context->level & (PRIVILEGE_SITE | PRIVILEGE_USER)) {
				error('%s works within context of site only', __FUNCTION__);

				return null;
			}
			$afi = \apnscpFunctionInterceptor::factory($context);
			$ports = $afi->ssh_port_range();
			$port = null;
			foreach ($ports as $tmp) {
				[$low, $high] = $tmp;
				for ($low; $low <= $high; $low++) {
					if (static::free($low)) {
						$port = $low;
						break;
					}
				}
			}

			return $port;
		}

		/**
		 * Verify if port is available
		 *
		 * @param int         $port
		 * @param null|string $ip
		 * @param string|null $family tcp/udp for both IPv4/IPv6. tcp4/tcp6, udp4/udp6 for family-specific
		 * @return bool
		 */
		public static function free(int $port, ?string $ip = null, string $family = 'tcp'): bool
		{
			if (!\in_array($family, ['tcp', 'udp', 'tcp6', 'udp6', 'tcp4', 'udp4'], true)) {
				return error("Unknown packet family `%s'", $family);
			}
			if (null === ($records = self::connections($port, $family))) {
				return true;
			}
			if (!$ip) {
				return false;
			}
			$ip = inet_pton($ip);
			foreach ($records as $record) {
				$localip = inet_pton($record['local_ip']);
				if (!$localip || $localip === $ip) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Get entries by port
		 *
		 * @param int    $port
		 * @param string $family
		 * @return array|null
		 */
		protected static function connections(?int $port, string $family): ?array
		{
			$records = [];
			if ($family === 'tcp' || $family === 'udp') {
				$family = [$family, "${family}6"];
			} else {
				$family = rtrim($family, '4');
			}
			$records = [];
			foreach ((array)$family as $fam) {
				if (!$fetched = self::stat(null, $fam)) {
					continue;
				}
				if ($port) {
					$fetched = array_filter($fetched, static function ($r) use ($port) {
						return $r['local_port'] === $port;
					});
				}
				$records = array_merge($records, $fetched);
			}

			return $records ?: null;
		}

		public static function fromPid(int $pid, string $family = 'tcp'): array
		{

			$inodes = [];
			Filesystem::readdir("/proc/${pid}/fd/", static function ($file) use (&$inodes, $pid) {
				$link = @readlink("/proc/${pid}/fd/${file}");
				if (!$link || 0 !== strncmp($link, 'socket:', 7)) {
					return null;
				}

				$inodes[] = (int)substr($link, 8, -1);
			});

			$net = self::stat($pid, $family);
			return array_values(array_filter($net, static function ($proc) use ($inodes) {
				if ($proc['remote_port']) {
					return false;
				}

				return \in_array($proc['inode'], $inodes, true);
			}));

		}

		/**
		 * List ports on process
		 *
		 * @param int|null $pid
		 * @param string   $family
		 * @return array|null
		 */
		public static function stat(?int $pid, string $family = 'tcp'): ?array
		{
			if ($family !== 'tcp' && $family !== 'tcp6' && $family !== 'udp' && $family !== 'udp6') {
				fatal("Unknown/invalid family specified `%s'", $family);
			}
			// IPv6/IPv4 may not be present
			$path = "/proc/${pid}/net/${family}";

			if (!file_exists($path)) {
				return null;
			}
			$entries = file($path, FILE_IGNORE_NEW_LINES);
			$connections = [];
			// header
			array_shift($entries);
			foreach ($entries as $entry) {
				$tmp = array_combine([
					'sl',
					'local_part',
					'rem_part',
					'st',
					'queue',
					'tr_part',
					'retrnsmt',
					'uid',
					'timeout',
					'inode',
					'extra'
				], preg_split('/\s+/', $entry, 11, PREG_SPLIT_NO_EMPTY));
				[$lip, $lport] = explode(':', $tmp['local_part']);
				$lport = (int)hexdec($lport);

				$tmp['sl'] = rtrim($tmp['sl'], ':');
				[$rip, $rport] = explode(':', $tmp['rem_part']);
				[$tx_queue, $rx_queue] = explode(':', $tmp['queue']);
				[$timer, $expiry] = explode(':', $tmp['tr_part']);
				// @link https://metacpan.org/pod/Linux::Proc::Net::TCP
				$tmp = [
						'family'      => $family,
						'local_ip'    => static::hex2ip($lip),
						'local_port'  => $lport,
						'remote_ip'   => static::hex2ip($rip),
						'remote_port' => (int)$rport,
						'tx_queue'    => hexdec($tx_queue),
						'rx_queue'    => hexdec($rx_queue),
						'inode'       => (int)$tmp['inode'],
						'sl'          => (int)$tmp['sl'],
						'st'          => hexdec($tmp['st']),
						't_active'    => (int)$timer,
						't_expiry'    => hexdec($expiry),
						'uid'         => (int)$tmp['uid'],
						'timeout'     => hexdec($tmp['timeout'])

					] + $tmp;
				unset($tmp['local_part'], $tmp['rem_part'], $tmp['queue'], $tmp['tr_part']);
				$connections[] = $tmp;
			}

			return $connections;
		}

		/**
		 * Convert a little-endian hex address to dotted quad
		 *
		 * @param string $hex
		 * @return string
		 */
		public static function hex2ip(string $hex): string
		{
			static $ipv6Cache = [];
			if (\strlen($hex) === 32) {
				// IPv6
				$key = hexdec($hex);
				if (!isset($ipv6Cache[$key])) {
					$ipv6Cache[$key] = inet_ntop(inet_pton(implode(':', array_map(static function ($str) {
						return implode(':', array_map(static function($quad) {
							return $quad[1] . $quad[0] . $quad[3] . $quad[2];
						}, str_split($str, 4)));
					}, array_map('strrev', str_split($hex, 8))))));
				}
				return $ipv6Cache[$key];
			}
			// IPv4
			return inet_ntop(hex2bin(implode('', array_reverse(str_split($hex, 2)))));
		}
	}