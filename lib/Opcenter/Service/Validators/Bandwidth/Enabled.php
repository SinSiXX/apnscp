<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Bandwidth;

	use Opcenter\Database\PostgreSQL;
	use Opcenter\Provisioning\Bandwidth;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\SiteConfiguration;


	/**
	 * Class Enabled
	 *
	 * @package Opcenter\Service\Validators\Siteinfo
	 *
	 * Critical account bootstrapping
	 */
	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall
	{
		use \FilesystemPathTrait;

		public const DESCRIPTION = 'Enable bandwidth logging and reports';

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			$pg = \PostgreSQL::pdo();
			$threshold = $this->ctx->getServiceValue(null, 'threshold');
			$stmt = $pg->prepare('INSERT INTO bandwidth (site_id, threshold, rollover) VALUES(:site_id, :threshold, :rollover)');
			if (!$stmt->execute([
				':site_id'   => $svc->getSiteId(),
				':threshold' => !$threshold ? null : (int)\Formatter::changeBytes($threshold, 'B',
					$this->ctx->getServiceValue(null, 'units', 'B')),
				':rollover'  => $this->ctx->getServiceValue(null, 'rollover')
			])) {
				return error('failed to populate bandwidth metadata: %s', $stmt->errorInfo()[2]);
			}

			$query = $this->vendorHandler()->createBandwidthSpan($svc->getSiteId());
			$pg->exec($query);

			Bandwidth::createModuleConfiguration($svc, 'bandwidth.apache-config');

			return true;
		}

		protected function vendorHandler(): PostgreSQL\Generic\Bandwidth
		{
			return PostgreSQL::vendor('bandwidth');
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$pg = \PostgreSQL::pdo();
			foreach (['bandwidth_spans', 'bandwidth_log', 'bandwidth_extendedinfo', 'bandwidth'] as $table) {
				$query = $pg->prepare("DELETE FROM ${table} WHERE site_id = :id");
				if (!$query->execute([':id' => $svc->getSiteId()])) {
					warn("failed to delete bandwidth data on `%s' for site `%d': %s", $table, $svc->getSiteId(),
						array_get($query->errorInfo(), 2, 'UNKNOWN'));
				}
			}

			$pg->exec($this->vendorHandler()->deleteExtendedBandwidth($svc->getSiteId(), null));
			Bandwidth::deleteModuleConfiguration($svc->getSite());

			return true;
		}
	}

