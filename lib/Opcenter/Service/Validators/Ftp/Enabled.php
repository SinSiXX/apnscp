<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ftp;

	use Opcenter\Provisioning\Ftp;
	use Opcenter\Provisioning\Pam;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall
	{
		use \FilesystemPathTrait;

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			(new Pam($svc))->enable(\Ftp_Module::PAM_SVC_NAME);
			Ftp::populateFilesystem($svc);

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			(new Pam($svc))->disable(\Ftp_Module::PAM_SVC_NAME)
				->terminate('vsftpd');

			return true;
		}
	}

