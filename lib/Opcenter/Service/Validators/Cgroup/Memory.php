<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Service\ServiceValidator;

	class Memory extends ServiceValidator
	{
		const DESCRIPTION = 'Limit memory usage of account in MB';
		const VALUE_RANGE = '<int>';

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('cgroup', 'enabled')) {
				$value = null;

				return true;
			} else if ($value === 0) {
				info('cgroup memory converted from `0\' to unenforced');
				$value = null;

				return true;
			}
			$converted = \Formatter::changeBytes((int)$value . 'MB', 'KB');
			$memavail = \Formatter::changeBytes(\Opcenter\System\Memory::stats()['memtotal'], 'MB', 'KB');
			if ($value > $memavail) {
				warn("requested cgroup memory `%s MB' exceeds available system memory - capping to `%s' MB",
					$value, $memavail
				);
				$value = $memavail;
			} else if (!$converted) {
				return false;
			}

			return true;
		}
	}
