<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Mail;

	use Opcenter\Mail\Storage;
	use Opcenter\Service\Contracts\AlwaysRun;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\SiteConfiguration;

	class Version extends \Opcenter\Service\Validators\Common\Version implements ServiceInstall, AlwaysRun
	{
		use \FilesystemPathTrait;

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			$pwd = $svc->getSiteFunctionInterceptor()->user_getpwnam($svc->getServiceValue('siteinfo', 'admin_user'));
			$path = $pwd['home'] . DIRECTORY_SEPARATOR . Storage::MAILDIR_HOME;
			$instance = Storage::bindTo($svc->getAccountRoot());
			if (!$instance->createMaildir($path, $pwd['uid'], $pwd['gid'])) {
				return error("failed to create Maildir storage in `%s'", $path);
			}
			foreach (Storage::BASE_FOLDERS as $folder) {
				$path = $pwd['home'] . DIRECTORY_SEPARATOR .
					Storage::MAILDIR_HOME . DIRECTORY_SEPARATOR .
					Storage::mailbox2Maildir($folder);
				$instance->createMaildir($path, $pwd['uid'], $pwd['gid']);

			}

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}
	}

