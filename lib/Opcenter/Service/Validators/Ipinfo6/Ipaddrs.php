<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ipinfo6;

	use Opcenter\Service\Validators\Common\IpValidation;
	use Opcenter\SiteConfiguration;

	class Ipaddrs extends IpValidation
	{
		const VALUE_RANGE = '[addr1, addr2...]';
		const DESCRIPTION = 'Assign IP addresses uniquely to account';

		public function valid(&$value): bool
		{
			if (!$this->ctx['enabled'] || $this->ctx['namebased']) {
				$value = [];
				return true;
			}

			if ($value && !isset($value[0])) {
				$value = array_key_map(static function ($k, $v) {
					return "$k:$v";
				}, (array)$value);
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}


	}
