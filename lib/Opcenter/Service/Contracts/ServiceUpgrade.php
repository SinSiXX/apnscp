<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Contracts;

	use Opcenter\SiteConfiguration;

	/**
	 * Upgrading service definition files
	 *
	 * @package Opcenter\Service\Contracts
	 */
	interface ServiceUpgrade
	{
		public function upgrade(string $target, string $src, SiteConfiguration $svc): bool;

		public function downgrade(string $target, string $src, SiteConfiguration $svc): bool;
	}