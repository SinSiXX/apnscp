<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Account;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Opcenter\SiteConfiguration;

	class Import extends DomainOperation implements Publisher
	{
		const HOOK_ID = 'vdimport';
		// @var string nasty kludge to ensure we have access to the backup
		const CALLBACK_HOOK_ID = 'cbvdimport';

		use \FilesystemPathTrait;

		// @var array options passed at runtime
		protected $runtimeConfiguration;
		// @var string backup filename
		protected $file;
		// @var int site id
		protected $site_id;
		// @var string "site" + id
		protected $site;
		// @var string domain
		protected $domain;

		// @var bool creation status, used to release site id on failure

		protected $options = [
			'reconfig' => false,
			// optional plan name
			'plan'     => null
		];

		/**
		 *
		 * @param string $file
		 * @param array  $options
		 */
		public function __construct(string $file, array $options = [])
		{
			// avoid firing callbacks on previously registered iterations
			Cardinal::purge();
			$this->file = realpath($file);
			$this->runtimeConfiguration = $options;
			cli_set_process_title(basename($_SERVER['argv'][0]) . ' ' . basename($file));
		}

		public function __destruct()
		{
			if ($this->status !== self::RC_FAILURE) {
				return;
			}
			if (\Error_Reporter::is_verbose(9999)) {
				pause("Restore failed on `%s'", $this->file);
			}
			Cardinal::fire([self::HOOK_ID, Events::FAILURE], $this);
		}

		/**
		 * Run account creation
		 */
		public function exec(): bool
		{
			$limit = ini_get('memory_limit');
			ini_set('memory_limit', '-1');
			Cardinal::register(
				[Import::CALLBACK_HOOK_ID, Events::SUCCESS],
				function ($event, Publisher $p) //SiteConfiguration $s, string $format, string $path)
				{
					$this->runUserHooks(...$p->getEventArgs());
				}
			);

			$ret = \Opcenter\Migration\Import::fromFile($this->file, $this->runtimeConfiguration);
			if (memory_get_usage(true) <= \Formatter::changeBytes($limit)) {
				ini_set('memory_limit', $limit);
			} else {
				debug('Memory held %d is greater than limit %s',
					\Formatter::changeBytes(memory_get_usage(true), 'MB'),
					\Formatter::changeBytes($limit, 'MB')
				);
			}
			// switchback and populate account configuration markers before
			// mount is called
			$this->status = $ret && !\Error_Reporter::is_error() ? static::RC_SUCCESS : static::RC_FAILURE;

			return $ret;
		}

		/**
		 * Set editor option
		 *
		 * @param string $option
		 * @param mixed  $val
		 * @return self
		 */
		public function setOption(string $option, $val): self
		{
			if (!\array_key_exists($option, $this->options)) {
				fatal("Unknown option `%s'", $option);
			}
			$this->options[$option] = $val;
		}

		/**
		 * Get domain name
		 *
		 * @return string|null
		 */
		public function getDomain(): ?string
		{
			return $this->domain;
		}

		public function getEventArgs()
		{
			return [
				'site'    => $this->site,
				'domain'  => $this->domain,
				'options' => $this->runtimeConfiguration
			];
		}
	}