<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Mail\Services\Postfix;
	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;
	use Opcenter\Role\User;
	use Opcenter\SiteConfiguration;

	class Mail
	{
		use FilesystemPopulatorTrait;
		use GroupCreationTrait;

		const TEMPLATE_FILES = [
		];
		const TEMPLATE_DIRECTORIES = [];
		const SUPPLEMENTAL_GROUPS = [
			'postfix'  => ['postfix', null],
			'postdrop' => ['postdrop', null]
		];

		public static function install(SiteConfiguration $svc): bool
		{
			static::createGroups($svc->getAccountRoot());
			User::bindTo($svc->getAccountRoot())->mirror('postfix');

			return true;
		}

		public static function uninstall(SiteConfiguration $svc): bool
		{
			$handler = User::bindTo($svc->getAccountRoot()) ;
			if ($handler->exists('postfix')) {
				$handler->delete('postfix');
			}

			foreach (Postfix::siteOverrides($svc->getSite()) as $file) {
				(is_file($file) && unlink($file)) || warn("Failed to remove `%s'", $file);
			}

			return true;
		}
	}
