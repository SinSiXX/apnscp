<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */


namespace Opcenter\Migration\Brokers;

abstract class Command {
	/**
	 * Run a command
	 *
	 * @param string $command
	 * @param mixed  ...$args
	 * @return array
	 */
	abstract public function run(string $command, ...$args): array;
}