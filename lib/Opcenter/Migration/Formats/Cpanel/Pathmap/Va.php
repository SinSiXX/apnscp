<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Notes\MailMan;
	use Opcenter\Migration\Notes\MailTransports;
	use Opcenter\Migration\Notes\User;
	use Opcenter\Migration\Remediator;
	use Opcenter\SiteConfiguration;

	class Va extends ConfigurationBuilder
	{
		const DEPENDENCY_MAP = [
			Userdata::class,
			Homedir::class,
			Mm::class
		];
		/**
		 * @var MailMan
		 */
		protected $mmList;
		/**
		 * @var User
		 */
		protected $userNote;

		public function parse(): bool
		{
			if (false === ($path = $this->getDirectoryEnumerator()->getPathname())) {
				return true;
			}

			$this->mmList = $this->bill->getNote(MailMan::class);
			$this->userNote = $this->bill->getNote(User::class);
			Cardinal::register([\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $s) {
					$remediator = Remediator::create($this->task->getRemediator($s)->getStrategy(), $s->getAuthContext());
					$afi = $s->getSiteFunctionInterceptor();
					// track domains explicitly listed to handle mail, remove leftover
					$allDomains = array_keys($afi->web_list_domains());

					foreach ($this->getDirectoryEnumerator() as $p) {
						$hostname = basename($p->getPathname());
						if (0 === strncmp($hostname, '*', 1)) {
							warn('%(brand)s does not support wildcard MX delivery. Ignoring delivery on %(hostname)s', ['brand' => PANEL_BRAND, 'hostname' => $hostname]);
							continue;
						}
						$allDomains = array_diff($allDomains, [$hostname]);
						if ($this->bill->getNote(MailTransports::class)->revoked($hostname)) {
							// no mail established for hostname
							if ($afi->email_transport_exists($hostname)) {
								$afi->email_remove_virtual_transport($hostname);
							}
							continue;
						}
						$components = $afi->web_split_host($hostname);
						$subdomain = $components['subdomain'];
						$domain = $components['domain'];

						if (!$afi->email_transport_exists($hostname) && !$afi->email_add_virtual_transport($domain, $subdomain)) {
							warn('Failed to add transport to %s - skipping email import',
								ltrim(implode('.', [$subdomain, $domain]), '.'));
							continue;
						}

						foreach (file($p->getPathname(), FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES) as $line) {
							[$addr, $dest] = array_map('trim', explode(':', $line, 2));
							$pos = strpos($dest, ':fail:');
							if (false !== $pos && $pos <= 2) {
								// no catch-all on account
								continue;
							}

							if (false !== strpos($dest, ':blackhole:')) {
								if ($addr[0] === '*') {
									continue;
								}
								$dest = 'site_blackhole';
							}

							// mailing list
							if ($this->isList($addr, $dest)) {
								// @TODO MailMan import
								info('mailman lists not supported, discarding %(email)s => %(destination)s',
									['email' => $addr, 'destination' => $dest]);
								continue;
							}

							// validate name contains domain
							$check = substr($addr, -\strlen($hostname));
							$addr = substr($addr, 0, -\strlen($hostname) - 1);
							if ($check === '*') {
								// catch-all
								if (false !== strpos($dest, '@')) {
									$mailbox = $afi->email_get_mailbox(...explode('@', $dest, 2));
									if ($mailbox && array_get($mailbox, 'type', null) === \Email_Module::MAILBOX_USER) {
										$dest = $afi->user_get_username_from_uid($mailbox['uid']);
									} else if (!MAIL_FORWARDED_CATCHALL || MAIL_DISABLED_FORWARDING) {
										// catch-all
										fatal('Catch-alls may not forward externally without setting ' .
											'[mail] => forwarded_catchall=true - offending line: %s', $line);
									}
								}
								$addr = '';
							} else if ($check !== $hostname) {
								fatal('Sanity check, line %(line)s: %(check)s !== %(expected)s',
									['line' => $line, 'check' => $check, 'expected' => $hostname]);
							}

							$oldUser = $dest;
							if ($this->userNote->hasChange() && $dest === $this->userNote->getOriginalUser()) {
								$newuser = (string)$this->userNote->getNewUser();
								$dest = $newuser;
							} else if (false === strpos($dest, '@')) {
								// forwarded email
								try {
									$cred = ['user' => $dest, 'hostname' => $hostname];
									$tmp = $remediator->store($dest, $cred);
									$dest = $tmp;
								} catch (\ValueError $e) {
									$conflictor = $remediator->getConflict($dest);
									fatal('Conflict discovered using strategy %(strategy)s: %(user)s@%(host)s conflicts with ambiguous delivery to %(old)s',
										['strategy' => $remediator->getStrategy(), 'user' => $addr, 'host' => $hostname, 'old' => $conflictor['user']]);
								}
							}

							if ($oldUser !== $dest) {
								if ($addr === '' && !$afi->user_exists($dest)) {
									// catch-all that is delivered locally to user without a passwd entry
									$dest = $oldUser;
									info('Merging mail for %(user)@%(host)s to %(old)s. No ' .
										'distinct passwd entry detected for user',
										['user' => $addr, 'host' => $hostname, 'old' => $oldUser]);
								} else {
									info('Converted %(user)s@%(host)s delivery (user: %(old)s => %(new)s)',
										['user' => $addr, 'host' => $hostname, 'old' => $oldUser, 'new' => $dest]);
								}
							}
							$exists = $afi->email_address_exists($addr, $hostname);
							$type = $dest !== 'site_blackhole' && false === strpos($dest, '@') && false === strpos($dest, ',') ?
								\Email_Module::MAILBOX_USER : \Email_Module::MAILBOX_FORWARD;
							// purge mailbox if exists
							if ($exists && !$afi->email_remove_mailbox($addr, $hostname, $type)) {
								return error("Failed to remove `%s'@`%s': %s", $addr, $hostname, $type);
							}

							if ($type === \Email_Module::MAILBOX_USER) {
								if (!$afi->user_exists($dest)) {
									return error("User `%s' not found in /etc/passwd", $dest);
								}
								if (\User_Module::MIN_UID >= ($uid = $afi->user_get_uid_from_username($dest))) {
									return error("User `%(user)s' has restricted user id %(uid)d - system user?",
										['user' => $dest, 'uid' => $uid]);
								}
								// physical mailbox
								if (!$afi->email_add_mailbox($addr, $hostname, $uid)) {
									return error("Failed to create mailbox for `%(user)s@%(host)s' (user: %(dest)s)",
										['user' => $addr, 'host' => $hostname, 'dest' => $dest]);
								}
							} else {
								if ($addr === '' && $this->task->getOption('drop-forwarded-catchalls')) {
									warn('Catch-all detected that forwards remotely - ignoring *@%(host)s => %(dest)s',
										['host' => $hostname, 'dest' => $dest]);
									continue;
								}
								if (false !== strpos($dest, 'bin/autorespond')) {
									// follow non-standard home directory locations
									$homedir = dirname((string)$this->bill->getNote(\Opcenter\Migration\Notes\Homedir::class)) ?:
										'/home/';
									if (!preg_match('!' . preg_quote($homedir, '!') . '([^/ ]+)!', $dest, $match)) {
										return warn('Unable to detect user in forward %s. Discarding.', $dest);
									}

									if ($this->userNote->hasChange() && $match[1] === $this->userNote->getOriginalUser()) {
										$match[1] = (string)$this->userNote->getNewUser();
									}

									warn("Autoresponder detected on `%(user)s@%(host)s'. Converting forward to " .
										"`%(dest)s'. Autoresponders behavior is incompatible with %(brand)s.",
									['user' => $addr, 'host' => $hostname, 'dest' => $match[1], 'brand' => PANEL_BRAND]);
									if ($afi->email_address_exists($addr, $hostname)) {
										return warn('Address %(user)@%(host)s exists - ignoring', ['user' => $addr, 'host' => $hostname]);
									}

									if (!$afi->email_add_mailbox($addr, $hostname, $afi->user_get_uid_from_username($match[1]))) {
										return error("Failed to create email address `%(user)s@%(host)s': %(dest)s",
											['user' => $addr, 'host' => $hostname, 'dest' => $match[1]]);
									}
									return true;
								}
								if (!$afi->email_add_alias($addr, $hostname, $dest)) {
									return error("Failed to create email alias `%(user)s@%(host)s': %(dest)s",
										['user' => $addr, 'host' => $hostname, 'dest' => $dest]);
								}
							}
						}
					}

					// remove domains for which no transport exists
					foreach ($allDomains as $hostname) {
						if (!$afi->email_transport_exists($hostname)) {
							continue;
						}
						if (!$afi->email_remove_virtual_transport($hostname)) {
							warn('Failed to remove transport on %s', $hostname);
						}
					}

					return true;
			});

			return true;
		}

		/**
		 * Specified address is mailing list
		 *
		 * @param string $name
		 * @param string $dest
		 * @return bool
		 */
		protected function isList(string $name, string $dest = ''): bool
		{
			return $this->mmList->in($name);
		}
	}