<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class FlareCheck implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg['apnscp_flare_check'] = $val;
			unset($cfg);
			Bootstrapper::run('apnscp/install-services');

			return true;
		}

		public function get()
		{
			$config = new Config();
			$val = $config['apnscp_flare_check'];

			return $val;
		}

		public function getHelp(): string
		{
			return 'Enable periodic emergency update checks';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return 'true';
		}
	}