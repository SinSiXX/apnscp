<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Admin\Settings\Argos;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Argos\Config;

	class Init implements SettingsInterface
	{
		public function set($val): bool
		{
			if (!$val && $this->get()) {
				return error('%s exists - supply 1 to argos.init to re-initialize configuration file',
					Config::CONFIGURATION_FILE);
			}

			return Config::initialize();
		}

		public function get()
		{
			return file_exists(Config::CONFIGURATION_FILE);
		}

		public function getHelp(): string
		{
			return 'Initialize Argos configuration';
		}

		public function getValues()
		{
			return 'boolean';
		}

		public function getDefault()
		{
			return false;
		}
	}