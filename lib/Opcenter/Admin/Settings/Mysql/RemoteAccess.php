<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mysql;

	use Opcenter\Admin\Bootstrapper\Config as BSConfig;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Database\DatabaseCommon;

	class RemoteAccess implements SettingsInterface
	{
		use \NamespaceUtilitiesTrait;

		public function set($val): bool
		{
			if ($val === $this->get()) {
				// no need to set the value again
				return true;
			}
			$cfg = new BSConfig();
			$cfg[$this->getDatabaseType() . '_remote_connections'] = (bool)$val;
			$cfg->sync();
			return \Opcenter\Admin\Bootstrapper::run($this->getDatabaseType() . '/install', 'network/setup-firewall', 'fail2ban/configure-jails');
		}

		public function get()
		{
			$cfg = new BSConfig();
			$state = $cfg[$this->getDatabaseType() . '_remote_connections'];
			if ($state !== '{{ allow_remote_db }}') {
				return $state;
			}
			return $cfg['allow_remote_db'];
		}

		private function getDatabaseType(): string
		{
			$ns = static::getNamespace();
			return strtolower(substr($ns, strrpos($ns, '\\')+1));
		}

		public function getHelp(): string
		{
			$type = $this->getDatabaseType();
			return sprintf(_('Enable remote %s connections'), DatabaseCommon::canonicalizeBrand($type));
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
