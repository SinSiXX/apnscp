<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Filelist;

	use CLI\Yum\Synchronizer\Plugins\Filelist;

	/**
	 * Class Sudo
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Filelist
	 *
	 * Override filelist
	 *
	 */
	class Sudo extends Filelist
	{
		const VULNERABLE_HASHES = [
			'711d1bf7dca61ad7550a289a0e37edda',
			'3a8943e8121721979550bd001ab25d5a'
		];
		/**
		 * Get updated filelist
		 *
		 * @param array $files
		 * @return array
		 */
		public function filterFiles(array $files): array
		{
			$myHash = md5_file('/usr/bin/sudo');
			return array_filter($files, static function ($v) use ($myHash) {
				return $v !== '/usr/bin/sudo' || !in_array($myHash, self::VULNERABLE_HASHES, true);
			});
		}
	}