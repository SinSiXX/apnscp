<div class="d-block d-sm-flex align-items-end">
	@include('master::partials.shared.search', [
		'filter' => new \apps\mailboxroutes\models\Search
	])
	<form method="get" action="{{ \HTML_Kit::page_url_params(['mode' => 'edit']) }}" class="mb-4 ml-auto position-relative">
		<button class="ml-auto ui-action-label ui-action ui-action-toolbox toolbox btn btn-secondary dropdown-toggle"
		        aria-haspopup="true" aria-expanded="false" type="button" data-toggle="dropdown">
			Toolbox
		</button>
		<div class="dropdown-menu dropdown-menu-right" id="toolbox">
			<button name="backup" class="btn btn-block dropdown-item" type="submit">
				<i class="ui-action ui-action-download"></i>
				Export Table
			</button>
			@if (\count($Page->get_transports()) > 1)
				<button name="clone" class="btn btn-block dropdown-item" type="button" id="cloneMailboxes">
					<i class="ui-action ui-action-copy"></i>
					Clone Mailboxes
				</button>
			@endif
		</div>
	</form>
</div>
<form method="POST" action="{{ \HTML_Kit::page_url_params(['mode' => 'edit']) }}">
	<h3>Address Table</h3>
	<table width="100%" id="mailbox_routes" class="multirow table tablesorter">
		<thead>
		<tr>
			<th class="text-center nosort">
				<input type="checkbox" id="select-all"/>
			</th>
			<th width="140">
				User
			</th>
			<th width="170">
				Domain
			</th>
			<th>
				Destination
			</th>
			<th class="center actions nosort" width="150">
				Actions
			</th>
		</tr>
		</thead>
		<tfoot>
		<tr>
			<td class="center" colspan="6">
				<p>Select multiple addresses from the <i class="fa fa-check-square-o"></i> column and perform an
					action:
				<div class="row">
					<div class="col-sm-12 col-md-4 offset-md-1">
						<button type="submit" name="sel_disable" value="Disable Selected Addresses"
						        class="btn btn-secondary">
							<i class="ui-action ui-action-disable"></i>
							Disable Selected Addresses
						</button>
					</div>
					<div class="col-sm-12 col-md-5 offset-md-1">
						<button type="submit" name="sel_delete" class="btn btn-secondary warn">
							<i class="ui-action ui-action-delete"></i>
							Delete Selected Addresses
						</button>
					</div>
				</div>
			</td>
		</tr>
		</tfoot>

		<tbody >
			@include('partials.address-list', ['accounts' => $Page->get_mailboxes('enabled'), 'listMode' => 'enabled'])
		</tbody>
	</table>

	<h4>Bulk Forwarding Tools</h4>
	<button class="btn btn-outline-secondary" data-toggle="collapse" href="#bulkTools" aria-expanded="false"
	        aria-controls="bulkTools">
		<i class="ui-action ui-action-properties"></i>
		Access Tools
	</button>
	<div class="collapse mt-1" id="bulkTools">
		<label for="new_address" class="hinted">Addresses will be applied to the selected entries above. Sample
			format:
			usera@example.com
			userb@domain.com, userc@example.com</label>
		<textarea name="new_address" rows="3" class="form-control" id="new_address"></textarea>

		<div class="row mt-1 mb-5">
			<div class="input-group  col-md-4 col-12">
				<div class="btn-group">
					<button type="submit" name="sel_append" value="Append Addresses" class="btn btn-secondary">
						Append Addresses
					</button>
					<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
					        aria-haspopup="true" aria-expanded="false">
						<span class="sr-only">Additional Options</span>
					</button>
					<div class="dropdown-menu">
						<button type="submit" name="sel_replace" value="Replace Addresses" class="dropdown-item">
							Replace Addresses
						</button>

						<button type="submit" name="sel_remove" value="Remove Matching Addresses"
						        class="dropdown-item warn">
							Remove Matching Addresses
						</button>

					</div>
				</div>
			</div>
			<div class="col-12">
				<p class="alert alert-info mt-1">
					All checked addresses above will have the addresses immediately above added , removed, or
					replaced to
					each address. Bulk Forwarding is only useful with forwarded email accounts.
				</p>
			</div>
		</div>
	</div>

	@include('partials.modals.edit-address')
</form>

@include('partials.modals.clone-addresses')