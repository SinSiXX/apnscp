@modal(['id' => "snapshotModal", 'title' => "Snapshot success!"])
<div>
	<div class="mb-1 alert alert-success">
		<i class="fa fa-database"></i>
		<span class="dbname"></span>
		has been saved to
		<span class="filename"></span>
	</div>
	<p class="note small">
		Remember to manually delete these files after use
		(Files &gt; <a class="ui-action ui-action-label ui-action-switch-app"
		               href="{!! \HTML_Kit::new_page_url_params('/apps/filemanager',
						   array('f' => \Util_Conf::home_directory() . '/pgsql_backups')) !!}">File
			Manager</a>)
		otherwise the snapshot will be automatically deleted after 5 days.
	</p>
</div>
@endmodal

@modal(['id' => 'importModal', 'title' => '', 'action' => \HTML_Kit::page_url()])
@slot('headerBody')
	<h4 class="modal-title db-name"></h4>
@endslot

<h5>Restore from backup</h5>
<div class="">
	<div class="form-group">
		<ul class="backup-restore list-unstyled mb-0" id="db-list">
		</ul>
		<p class="note">
			No backups configured for <span class="db-name"></span>. Configure in
			<a href="/apps/pgsqlbackup" class="ui-action ui-action-label ui-action-switch-app">PostgreSQL
				Backups</a>.
		</p>
		<div class="help-block with-errors text-danger"></div>
	</div>
	<hr/>
	<fieldset class="form-group">
		<label class="custom-control custom-checkbox mb-0">
			<input name="enable" type="checkbox" class="custom-control-input" value="1" id="confirm"
			       required/>
			<span class="custom-control-indicator"></span>
			<span>
					I understand that importing a database backup will delete all
					data presently in <b class="db-name"></b>. This cannot be undone.
				</span>
		</label>
		<div class="help-block with-errors text-danger"></div>
	</fieldset>
</div>
@slot('buttons')
	<button type="submit" name="export[]" value="1" data-validate="false"
	        class="btn btn-secondary export">
		<i class="fa fa-download"></i>
		Export Database
	</button>
	<button type="submit" class="btn btn-primary import ajax-wait">
		<i class="fa fa-pulse"></i>
		Import
	</button>
	<p class="ajax-response"></p>
@endslot
@endmodal
