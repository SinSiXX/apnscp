<form method="POST" action="{{ \HTML_Kit::page_url_params() }}">
	<div class="row clearfix">
		<div class="col-6 col-md-3 col-lg-3">
			<label class="col-12">&nbsp;</label>
			<input type="hidden" name="state" value="{{ base64_encode(serialize($user)) }}"/>

			<span class="form-control-static">
				<i class="fa fa-user"></i>
				{{ $user }}
			</span>
		</div>

		<div class="col-6 col-md-3 col-lg-3">
			<label>Password</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-lock"></i>
				</span>
				<input type="password" class="form-control" name="password" value=""/>
			</div>
		</div>

		<div class="col-6 col-md-3 col-lg-2">
			<label>Max Connections</label>
			<div class="row">
				<div class="col-6 col-sm-6">
					<input type="text" class="form-control" name="max_connections" maxlength="2" size="2"
					       value="{{ $connlimit['max_connections'] }}"/>
				</div>
			</div>

		</div>

		<div class="col-6 col-md-3 col-lg-4 text-right">
			<label class="col-12">&nbsp;</label>
			<div class="btn-group">
				<button type="submit" value="Save" name="save_changes" class="btn btn-primary">
					Save
				</button>
				@if ($user !== $_SESSION['username'])
					<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
					        aria-haspopup="true" aria-expanded="false">
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<div class="dropdown-menu dropdown-menu-right">
						<button type="submit"
						        class="btn-block ui-action ui-action-label warn ui-action-delete dropdown-item"
						        name="d" value="{{ base64_encode(serialize($user)) }}"
						        onClick="return confirm('Are you sure you want to delete the user {{ $user }}?');">
							Delete User
						</button>
					</div>
				@endif
			</div>
		</div>
	</div>
</form>
<hr/>