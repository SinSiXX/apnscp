<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\crontab;

	use Cron;
	use Page_Container;

	class Page extends Page_Container
	{
		const TEE_PREFIX = "tee";

		public function __construct()
		{
			parent::__construct();
			$this->add_javascript('crontab.js');
			$this->add_javascript('/js/ajax_unbuffered.js');
			$this->add_css('crontab.css');

		}

		public function on_postback($params)
		{
			if (isset($params['save'])) {
				$this->crontab_set_mailto($params['mailto'] ?: null);
				if ($this->getCronjobStatus() != (bool)$params['toggle_cronjob']) {
					$this->toggleCronjob($params['toggle_cronjob']);
				}
				return;
			}
			// fix for IMAGE input type handled differently in IE/Opera

			if (isset($params['Edit_Cronjob'])) {
				$this->hide_pb();
			} else if (isset($params['Save_Edit'])) {
				if (!isset($params['minute']) || $params['minute'][0] != '@') {
					foreach (array('minute', 'hour', 'dom', 'month', 'dow') as $postbackParam) {
						if (!preg_match('/\*+|[[:digit:]]+|[[0-9]\\\*]+\/[[0-9]\\\*]+$/', $params[$postbackParam])) {
							error("Invalid expression for " . $postbackParam);
						}
					}
					$params['time'] = 'custom';
				} else {

					$params['hour'] = $params['dom'] =
					$params['month'] = $params['dow'] = $params['time'] = '';
				}
				if ($this->errors_exist()) {
					return false;
				}
				$oldjob = \Util_PHP::unserialize(base64_decode($params['Original_State'][$params['Save_Edit']]));
				$this->deleteCronjob($oldjob);
				if (!$this->addCronjob($params['time'], $params['cmd'])) {
					$this->addCronjob($oldjob['time'], $params['cmd']);
				}
				$location = \HTML_Kit::page_url_params();

				header('Location: ' . $location, true, 302);
				exit();
			} else if (isset($params['Add_Cronjob'])) {
				$this->addCronjob($params);
			} else if (isset($params['disable'])) {
				$this->disableCronjob($params['time'], $params['cmd']);
			} else if (isset($params['enable'])) {
				$this->enableCronjob($params['time'], $params['cmd']);
			} else if (isset($params['delete'])) {
				$this->deleteCronjob(
					$params['time'], $params['cmd']
				);
			}
		}

		public function toggleCronjob($value)
		{
			$this->crontab_toggle_status($value);
		}

		public function deleteCronjob($time, $cmd)
		{
			$min = $hour = $dom = $month = $dow = null;
			if ($time[0] == '@') {
				$min = $time;
			} else {
				list($min, $hour, $dom, $month, $dow) = preg_split('/\s+/', $time);
			}

			return $this->crontab_delete_job($min, $hour, $dom, $month, $dow, $cmd);
		}

		public function addCronjob($time, $cmd = null)
		{
			if (is_array($time)) {
				$params = $time;
				if (!isset($params['time'])) {
					return error("no scheduling option set");
				} else if (!isset($params['time']) && !isset($params['minute'])) {
					return error("no scheduling option set");
				} else if (isset($params['time']) && !isset($params['minute'])) {
					$params['minute'] = $params['time'];
				}
				foreach (array('minute', 'hour', 'dom', 'month', 'dow') as $ts) {
					if (!array_key_exists($ts, $params)) {
						$params[$ts] = '';
					}
				}
				$time = $params['time'];
				$cmd = $params['cmd'];
			}
			$min = $hour = $dom = $month = $dow = null;
			if ($time === "@custom" && isset($params)) {
				$min = $params['minute'];
				$hour = $params['hour'];
				$dom = $params['dom'];
				$month = $params['month'];
				$dow = $params['dow'];
			} else if ($time[0] === '@') {
				$min = $time;
			} else {
				list($min, $hour, $dom, $month, $dow) = preg_split("/\s+/", $time);
			}
			return $this->crontab_add_job(
				$min, $hour, $dom, $month, $dow,
				$cmd
			);
		}

		public function disableCronjob($timespec, $cmd)
		{
			$min = $hour = $dom = $month = $dow = "";
			if ($timespec[0] === "@") {
				$min = $timespec;
			} else {
				list($min, $hour, $dom, $month, $dow) = preg_split("/\s+/", $timespec);
			}

			return $this->crontab_disable_job(
				$min, $hour, $dom, $month, $dow, $cmd
			);
		}

		public function enableCronjob($timespec, $cmd)
		{
			$min = $hour = $dom = $month = $dow = "";
			if ($timespec[0] === "@") {
				$min = $timespec;
			} else {
				list($min, $hour, $dom, $month, $dow) = preg_split("/\s+/", $timespec);
			}

			return $this->crontab_enable_job(
				$min, $hour, $dom, $month, $dow, $cmd
			);
		}

		public function getCronjobStatus()
		{
			return $this->crontab_enabled();
		}

		public function canSchedule()
		{
			return $this->crontab_permitted();
		}

		public function get_cronjob_data()
		{
			return $this->crontab_list_jobs();
		}

		public function formatCron($timespec)
		{
			try {
				$cron = Cron\CronExpression::factory($timespec);
			} catch (\Exception $e) {
				$cron = new class
				{
					public function getNextRunDate()
					{
						return 'ON BOOT';
					}

					public function getPreviousRunDate()
					{
						return (new \DateTime())->setTimestamp(time() - \apnscpFunctionInterceptor::init()->call('common_get_uptime',
								[false]));
					}
				};
			} catch (\Throwable $e) {
				return null;
			}
			try {
				$next = $cron->getNextRunDate();
			} catch (\RuntimeException $e) {
				// implausible timespec
				return null;
			}

			return array(
				'next' => $next instanceof \DateTime ? $next->format('Y-m-d H:i') : $next,
				'last' => $cron->getPreviousRunDate()->format('Y-m-d H:i')
			);
		}

		public function getMailto(): ?string
		{
			return $this->crontab_get_mailto();
		}

		public function read($file, $offset = 0)
		{
			if ($file[0] == '/' || !ctype_alnum(substr($file, strlen(self::TEE_PREFIX)))) {
				return error("invalid tee file `%s'", $file);
			} else if (!ctype_digit($offset)) {
				return error("invalid offset `%s'", $offset);
			}
			$file = TEMP_DIR . '/' . $file;

			return \Util_Process_Tee::read($file, $offset);
		}

		public function reserve_tee()
		{
			$tmp = TEMP_DIR;
			$tempnam = tempnam($tmp, self::TEE_PREFIX);
			unlink($tempnam);

			return substr($tempnam, strlen($tmp) + 1);
		}

		public function run($cmd, $tee)
		{
			if (strncmp($tee, self::TEE_PREFIX, strlen(self::TEE_PREFIX))
				|| !ctype_alnum(substr($tee, strlen(self::TEE_PREFIX)))) {
				return error("invalid tee file");
			}
			$teefile = TEMP_DIR . '/' . $tee;
			if (file_exists($teefile) && !unlink($teefile)) {
				// unlink, pman_run will check to see if the tee does not exist before
				// allowing the param
				return error("tee file not created by WS?");
			}

			return $this->pman_run($cmd, null, null, array('tee' => $teefile));
		}
	}

?>
