<!-- main object data goes here... -->
<style type="text/css">
	select {
		width: 200px;
	}

	select:first-child {
		text-align: center;
	}
</style>

<h3>WebDisk URI</h3>
<div class="row mb-1">
	<p class="col-12 mb-0">
		Use the following URI to connect to WebDisk:
	</p>
	<div class="input-group col-md-6">
		<input type="text" id="referralURL" class="form-control" aria-describedby="clipboardCopy"
		       value="https://{{$Page->formatUri()}}:2078/"/>
		<span class="input-group-addon" id="clipboardCopy" data-toggle="tooltip" data-clipboard-target="#referralURL"
		      title="Copy to Clipboard"><i class="fa fa-clipboard"></i></span>
	</div>
</div>

<div class="alert alert-info">
	Your login is the same as all other logins:
	<p class="mt-3 mb-0">
		Login: <b><?=rtrim($_SESSION['username'] . '@' . $_SESSION['domain'], '@')?></b><br/>
		Password: &lt;your account password&gt;
	</p>
</div>

<a href="https://{{$Page->formatUri()}}:2078/" class="btn btn-primary ml-0 mr-2 mt-2 mb-2">Access Web Disk Now</a>

<div class="mt-3">
	<h4>OS Instructions</h4>
	<ul>
		<li class="">
			Windows <a href="{{ $Page->var('winscp') }}">WinSCP</a> or <a
					href="{{ $Page->var('webdrive') }}">WebDrive</a>
		</li>
		<li>
			<a href="{{ $Page->var('macos') }}">macOS</a>
		</li>
		<li>
			<a href="{{ $Page->var('linux') }}">Linux (DAVfs)</a>
		</li>
	</ul>
</div>