<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Page extends Page_Container
	{
		private $offsite;

		public function __construct()
		{
			\Page_Renderer::hide_all();
			$this->add_css('.ui-app { margin:5em; } ' .
				'.ui-app .ui-action-visit-site { display: block; clear: both; margin: 0 auto; } ' .
				'.ui-app .ui-infobox-info { width: 450px; margin: 0 auto; }', 'internal');
		}

		public function on_postback($params)
		{
			$this->hide_pb();
			if (!isset($params['app'])) {
				return false;
			}
			$app = $params['app'];
			if ($app[0] == '/') {
				header('Location: ' . $app, true, 302);
				exit;
			}
			$this->offsite = $app;
		}

		public function isOffsite()
		{
			return $this->offsite != "";
		}

		public function getOffsite()
		{
			return $this->offsite;
		}
	}

?>