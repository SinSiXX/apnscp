@component('partials.generic-service', ['service' => "FTP", 'auth' => $auth])
	@slot("extra")
		<tfoot>
		<tr>
			<td colspan="2">
				<h6>Notes</h6>
				FTP server supports explicit SSL (Auth TLS/STARTTLS).
				@if ($auth->hasSsh())
					Alternatively, SFTP may be used which relies on SSH to encrypt traffic.
				@endif
			</td>
		</tr>
		</tfoot>
	@endslot
@endcomponent