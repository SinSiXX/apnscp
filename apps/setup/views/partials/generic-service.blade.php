@include('partials.about', ['service' => $service])

<table class="table table-responsive table-striped">
	<tr>
		<th>
			{{ _("Hostname") }} ({{ _("IP") }})
		</th>
		<td>
			{{ $auth->hostname($service) }} ({{ $auth->ipAddress() }})
		</td>
	</tr>
	@isset($port)
	<tr>
		<th>
			{{ _("Port") }}
		</th>
		<td>
			{{ $port }}
		</td>
	</tr>
	@endisset
	@include('partials.auth.login')
	{{ $extra ?? '' }}
</table>

