<tr>
	<th>
			<span class="ui-action-copy" data-clipboard-text="{{ MAIL_DEFAULT_SPF  }}">
				<b class="text-uppercase" title="Copy to Clipboard" id="clipboardCopySpf"
				   data-toggle="tooltip"></b>
				SPF
			</span>
	</th>
	<td>
		<code>{{ MAIL_DEFAULT_SPF }}</code>
	</td>
</tr>