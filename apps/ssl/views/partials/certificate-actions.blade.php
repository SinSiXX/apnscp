<div class="btn-group">
	<button class="btn btn-secondary" id="download" name="download"
	        value="{{ $certificate->getName() }}">
		<i class="fa fa-download"></i>
		Download Certificate + Key
	</button>
	<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
	        aria-haspopup="true" aria-expanded="false">
		<span class="sr-only">Toggle Dropdown</span>
	</button>
	<div class="dropdown-menu dropdown-menu-right" aria-labelledby="#dropdownCertificateOptions">
		<button class="dropdown-item btn btn-block" id="generate" data-toggle="collapse"
		        data-target="#create-certificate" aria-expanded="false" aria-controls="create-certificate"
		        name="generate" value="{{ $certificate->getName() }}">
			<i class="fa fa-file-o"></i>
			Generate Request (CSR)
		</button>
		@if (is_debug())
			<button class="dropdown-item btn btn-block" id="replace-prompt" name="replace-prompt"
			        value="{{ $certificate->getName() }}">
				<i class="fa fa-files-o"></i>
				Replace Certificate
			</button>
		@endif
		<button class="dropdown-item btn warn btn-block" id="delete" name="delete"
		        value="{{ $certificate->getName() }}">
			<i class="fa fa-times"></i>
			Delete
		</button>
	</div>
</div>
<!--<div id="replace-container">
	Upload your new certificate.
</div>-->