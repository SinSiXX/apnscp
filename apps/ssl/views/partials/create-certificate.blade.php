@include('partials.hostname-generator')
<div id="create-certificate" class="mt-3">
	<h4>Certificate Request</h4>
	<div class="row">
		<label class="col-12" for="hostname">Hostname</label>
		<div class="input-group col-12 col-md-8 hostname-generator mb-1" id="hostnameGenerator">
			<span class="input-group-addon hidden-xs-down text-right">http://</span>
			<select class="custom-select form-control" name="subdomain" id="subdomain">
				<option value="*" class="dns-hide">*</option>
				<option value="">&lt;None&gt;</option>
				@foreach ($Page->get_subdomains() as $subdomain)
					<option value="{{ $subdomain }}">{{ $subdomain }}</option>
				@endforeach
			</select>
			<span class="input-group-addon text-center">.</span>
			<select class="custom-select form-control" name="domain" id="domain">
				@foreach ($Page->get_aliases() as $domain)
					<option value="{{ $domain }}">{{ $domain }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-12 col-md-4">
			<button class="btn btn-primary ui-action ui-action-label ui-action-add" id="addHostname" type="button">
				Add Hostname
			</button>
		</div>

		<!--<div class="col-12 col-md-4">
			<button type="button" name="add" id="add" class="btn btn-secondary ui-action ui-action-label ui-action-add">Add</button>
		</div>-->
	</div>
	<fieldset class="form-group">
		<label for="url"></label>
		<span class="text-muted" id="sample-url">SSL URL:
			<span>https://<span id="ex-subdomain"></span><span id="ex-domain"></span></span>
		</span>
	</fieldset>

	<!-- old self-signed garbage or CSR -->
	<div id="locale" class="collapse">
		@include('partials.certificate-information')
	</div>

	<fieldset class="form-group">
		<label for="email">Email Address</label>
		<input class="form-control" type="text" name="e"
		       value="{{ \Util_Conf::call('common_get_admin_email') }}" id="email"/>
	</fieldset>

	@includeWhen($Page->useLetsEncrypt(), 'partials.letsencrypt-options')
	@if ($Page->useLetsEncrypt())
		<button name="generate" value="lecert" id="generate-request" type="submit" class="btn main">
			<i class="fa fa-wand"></i>
			Generate &amp; Install
		</button>
	@else
		<button name="generate" value="csr" id="generate-request" type="submit" class="btn main">
			<i class="fa fa-wand"></i>
			Generate
		</button>
	@endif
</div>