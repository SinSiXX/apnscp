<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\whitelist;

	use Opcenter\Net\IpCommon;
	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->add_javascript('whitelist.js');
			$this->add_css('whitelist.css');
		}

		public function on_postback($params)
		{
			return $this->dispatchPostback($params);
		}

		public function _render()
		{
			$this->view()->share([
				'whitelist' => $this->getWhitelist(),
			]);
		}

		/**
		 * Get whitelist
		 *
		 * @return array
		 */
		public function getWhitelist(): array
		{
			if (\UCard::is('site')) {
				return $this->rampart_get_delegated_list();
			}

			// whitelist may not exist yet
			$map = \Error_Reporter::silence(function () {
				return $this->admin_read_map('whitelist');
			});
			array_walk($map, function (&$v, $k) {
				$v = explode(' ', $v);
			});

			return $map;
		}

		// controller methods
		protected function handleAdd(array $params) {
			$ip = $params['ip'] ?? '0';
			if (!IpCommon::valid($ip)) {
				return error('Invalid IP address');
			}
			if (IpCommon::reserved($ip)) {
				return error("Specified address `%s' is an internal/non-public IP address", $ip);
			}
			return $this->rampart_whitelist($ip);
		}

		protected function handleRemove(array $params) {
			return $this->rampart_whitelist($params['remove'], 'remove');
		}

		public function lookup(string $ip) {
			static $map = [];
			if (!isset($map[$ip])) {
				$map[$ip] = $this->dns_gethostbyaddr_t($ip);
			}
			return $map[$ip];
		}
	}