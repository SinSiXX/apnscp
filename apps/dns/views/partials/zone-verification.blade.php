<p class="lead">
	Domain pending verification. Management unavailable until domain is verified. Complete one of the following:
</p>
<ul class="">
	@foreach ($Page->dns_challenges($Page->getDomain()) as $type => $challenge)
		<li> @include('partials.challenges.' . $type, ['domain' => $Page->getDomain(), 'challenge' => $challenge])</li>
	@endforeach
</ul>

<form method="post" action="{{ \HTML_Kit::new_page_url_params(null, ['domain' => $Page->getDomain() ]) }}">
	<button type="submit" name="verify" value="{{ $Page->getDomain() }}" class="btn btn-primary mr-3">
		{{ _("Verify domain") }}
	</button>
	<a href="#" class="btn btn-secondary" data-toggle="collapse" data-placement="left" data-target="#domainDetails"
	   aria-controls="#domainDetails" aria-expanded="false" title=""
	   data-original-title="Show advanced information about domain">
		Show DNS details
	</a>
</form>