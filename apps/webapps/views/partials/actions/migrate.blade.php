<button data-note-target="#migrateHelp" type="submit" class="mb-3 btn btn-secondary" id="migrate"
        name="migrate" value="{{ $app->getDocumentMetaPath() }}">
	<i class="ui-action ui-action-label ui-action-rename ui-action-d-compact"></i>
	Rename Site
</button>

@include('partials.modals.migrate')