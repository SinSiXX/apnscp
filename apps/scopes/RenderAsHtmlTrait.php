<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace apps\scopes;

use Illuminate\Contracts\Support\Htmlable;

trait RenderAsHtmlTrait {
	public function asHtml(string $help) {
		return new class($help) implements Htmlable {
			protected $help;

			public function __construct($help)
			{
				$this->help = $help;
			}

			public function toHtml()
			{
				return $this->help;
			}

		};
	}
}


