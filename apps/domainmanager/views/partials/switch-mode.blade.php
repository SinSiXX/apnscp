<div class="row">
	<div class="col-12 text-right">
		<label>
			Change Mode
		</label>
		@if ($Page->getMode() == 'add')
			<a class="ui-action ui-action-switch-app ui-action-label"
			   href="{{ HTML_Kit::page_url_params(['mode' => 'view']) }}"
			>View/Edit Domains</a>
		@else
			<a class="ui-action ui-action-switch-app ui-action-label"
			   href="{{  \HTML_Kit::page_url_params(['mode' => 'add']) }}"
			>Add Domains</a>
		@endif
	</div>
</div>
