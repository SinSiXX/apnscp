# Templated from {{ $templatePath }}
[Unit]
Description=PHP-FPM pool activation {!! $config->getGroup() !!} - {!! $config->getName() !!}
PropagatesReloadTo={!! $config->getServiceName() !!}
BindsTo=@if ($config->getGroup()) {!! $config->getServiceGroupName() !!} @else php-fpm.service @endif

Before={!! $config->getServiceName() !!}

[Socket]
ListenStream={{ $config->getSocketPath() }}
SocketUser={{ \Web_Module::WEB_USERNAME }}
SocketGroup={{  $config->getSysGroup() }}
SocketMode=0660
RemoveOnStop=yes
DirectoryMode=1111

[Install]
WantedBy=sockets.target @if ($config->getGroup()) {!! $config->getServiceGroupName() !!} @endif
