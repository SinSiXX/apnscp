@component('mail::message')
# Hello,

Your trial for {{ PANEL_BRAND }} expires in {{ $expire }} {{\Illuminate\Support\Str::plural('day', $expire)}} on
<b>{{ $ip }}</b>. Purchase a license from [my.apiscp.com](https://my.apiscp.com) before
<b>{{ (new \Carbon\Carbon())->addDays($expire)->toDateString() }}</b> to avoid interruption.

---

**SERVER ADDRESS**: {{ $ip }}<br />
**HOSTNAME**: {{ $hostname }}<br />
**URL**: [{{ $secure_link }}]({{ $secure_link }})

@component('mail::button', ['url' => 'https://my.apiscp.com'])
	Purchase License
@endcomponent

---

Once a trial ends your sites will remain operational and firewall protection in place;
however, {{ PANEL_BRAND }} won't be able to automatically update your Web Apps or help
you manage your sites.

Thank you for taking the time to evaluate ApisCP. If you have any feedback let us know
at [help@apiscp.com](help@apiscp.com).

[Community Chat](https://discord.gg/wDBTz6V) · [Documentation](https://docs.apiscp.com) · [Blog](https://hq.apiscp.com)

@endcomponent

