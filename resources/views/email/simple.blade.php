@section("title", "Message from " . PANEL_BRAND)
@isset($job)
	@if (!$job->hasErrors())
		@component('email.indicator', ['status' => 'success'])
			Operation successful
		@endcomponent
	@else
		@component('email.indicator', ['status' => 'error'])
			Operation failed
		@endcomponent
	@endif
@endif
@component('mail::message')
	{{ $msg }}
@endcomponent
