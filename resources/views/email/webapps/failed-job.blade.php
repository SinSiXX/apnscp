## Note on failures
One or more jobs have failed. These will not be automatically updated until you clear the failed status within Launchpad. To clear the status for each failed application follow these steps:

* Login to [{{PANEL_BRAND}}]({{ \Auth_Redirect::getPreferredUri() }})
* Go to **Web** > **Web Apps**
* Find the failed application, click **Select**
* Under _App Meta_ > _Version_, click **Update**
* Upon successful update, the failed flag will be automatically reset