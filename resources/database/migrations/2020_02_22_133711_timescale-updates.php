<?php

use CLI\Yum\Synchronizer\Plugins\Trigger\TimescaledbPostgresqlAnyVersion;
use Illuminate\Database\Migrations\Migration;
use Opcenter\Database\PostgreSQL;

class TimescaleUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	// TimescaleDB gets loaded via Laravel
		$version = PostgreSQL::version();
		$maj = floor($version / 10000);
		$min = floor(($version % 1000) / 100);
		if ($maj >= 10) {
			$verPath = $maj;
		} else {
			$verPath = $maj . '.' . $min;
		}

		foreach (['timescaledb-2-postgresql-' . $verPath, 'timescaledb-postgresql-' . $verPath] as $tsdb) if (\CLI\Yum\Synchronizer\Utils::exists($tsdb)) {
			break;
		}
		(new TimescaledbPostgresqlAnyVersion())->update($tsdb);
		$pdo = \PostgreSQL::pdo();
    	if (TELEMETRY_ARCHIVAL_COMPRESSION) {
			$pdo->exec('ALTER TABLE metrics SET (timescaledb.compress, timescaledb.compress_segmentby = \'site_id,attr_id\');');
			(new \Daphnie\Chunker($pdo))->setCompressionPolicy(TELEMETRY_COMPRESSION_THRESHOLD);
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
