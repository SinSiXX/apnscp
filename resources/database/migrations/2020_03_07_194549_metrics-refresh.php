<?php

	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MetricsRefresh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		DB::connection('pgsql')->getPdo()->exec("DROP VIEW IF EXISTS metrics_view");
		foreach(['metrics_value_view', 'metrics_monotonic_view'] as $view) {
			DB::connection('pgsql')->getPdo()->exec("DROP MATERIALIZED VIEW IF EXISTS $view CASCADE");
		}

		$schema = function (Blueprint $table) {
			$table->integer('attr_id', false);
			$table->integer('site_id', false, true)->nullable();
			$table->timestampTz('ts', 0);
			$table->integer('value')->comment('Metric value');
			$table->foreign('site_id')->references('site_id')->on('siteinfo')
				->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('attr_id')->references('attr_id')->on('metric_attributes')
				->onUpdate('cascade')->onDelete('cascade');
		};

		Schema::connection('pgsql')->dropIfExists('metrics2');
		/**
		 * @XXX SELECT INTO copies a schemaless layout... replicate schema then use INSERT INTO
		 */
		Schema::connection('pgsql')->create('metrics2', $schema);

		DB::connection('pgsql')->getPdo()->exec("INSERT INTO metrics2 SELECT * FROM metrics");
		// recreate "metrics"
		Schema::connection('pgsql')->dropIfExists('metrics');
		Schema::connection('pgsql')->create('metrics', $schema);

		DB::connection('pgsql')->getPdo()->exec("ALTER TABLE metrics ADD CONSTRAINT
			unique_metric_check UNIQUE (site_id, attr_id, ts)");

		DB::connection('pgsql')->getPdo()->exec("SELECT create_hypertable('metrics', 'ts', chunk_time_interval  => interval '1 day');");

		DB::connection('pgsql')->getPdo()->exec("SELECT add_dimension('metrics', 'site_id', number_partitions => 4);");
		//DB::connection('pgsql')->getPdo()->exec("CREATE INDEX site_id_idx ON metrics2 USING BRIN(site_id,attr_id) WITH (timescaledb.transaction_per_chunk);");

		DB::connection('pgsql')->getPdo()->exec("INSERT INTO metrics SELECT * FROM metrics2");
		Schema::connection('pgsql')->dropIfExists('metrics2');

		DB::connection('pgsql')->getPdo()->exec('ALTER TABLE metrics SET (timescaledb.compress, timescaledb.compress_segmentby = \'site_id,attr_id\');');
		if (TELEMETRY_ARCHIVAL_COMPRESSION) {
			(new \Daphnie\Chunker(DB::connection('pgsql')->getPdo()))->setCompressionPolicy(TELEMETRY_COMPRESSION_THRESHOLD);
		}
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
