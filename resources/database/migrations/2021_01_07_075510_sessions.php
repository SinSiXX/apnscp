<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Schema;

class Sessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	// in appldb for installation
		if (!Schema::connection('pgsql')->hasTable(apnscpSession::TABLE)) {
			Schema::connection('pgsql')->create(apnscpSession::TABLE, function (Blueprint $table) {
				$table->char('session_id', 32);
				$table->string('domain')->nullable();
				$table->string('username', 32)->nullable();
				$table->tinyInteger('level', false, true)->default(0);
				$table->dateTimeTz('login_ts')->default(DB::raw('CURRENT_TIMESTAMP'));
				$table->timestampTz('last_action')->default(DB::raw('CURRENT_TIMESTAMP'));
				$table->binary('session_data');
				$table->integer('user_id')->unsigned()->nullable();
				$table->integer('group_id')->unsigned()->nullable();
				$table->tinyInteger('site_id')->nullable()->unsigned();
				$table->enum('login_method', ['http','soap','dav','cli']);
				$table->boolean('auto_logout')->default(true);
				$table->primary('session_id');
				$table->index('last_action', 'la_idx');
				$table->index('site_id', 'lookup_by_site');
				$table->foreign('site_id')->references('site_id')->on('siteinfo')->onDelete('cascade');
			});
			DB::connection('pgsql')->statement(
				"CREATE TYPE login_method AS ENUM('" .
				implode("','", ['http', 'soap', 'dav', 'cli']) . "')"
			);
			DB::connection('pgsql')->statement(
				"ALTER TABLE " . apnscpSession::TABLE . " ALTER login_method TYPE login_method USING login_method::login_method"
			);
			DB::connection('pgsql')->statement(
				"ALTER TABLE " . apnscpSession::TABLE . " ALTER login_method SET DEFAULT 'http'::login_method"
			);
		}

		\Opcenter\Apnscp::restart('now');
		sleep(30);
		// unusable; requires session support to send a session
		//\Opcenter\Apnscp::wait();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('pgsql')->hasTable(apnscpSession::TABLE)) {
        	Schema::connection('pgsql')->drop(apnscpSession::TABLE);
		}
		DB::connection('pgsql')->statement("DROP TYPE IF EXISTS login_method");
    }
}
