<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BandwidthSpanIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	// purge duplicate keys
    	\Illuminate\Support\Facades\DB::connection('pgsql')->getPdo()->
			exec('WITH cte AS (DELETE FROM bandwidth_spans RETURNING *), ctein AS (SELECT site_id, begindate, enddate, row_number() OVER (PARTITION BY site_id, begindate ORDER BY begindate) rank FROM cte) ' .
				'INSERT INTO bandwidth_spans SELECT site_id, begindate, enddate FROM ctein WHERE rank=1;');
		\Illuminate\Support\Facades\DB::connection('pgsql')->getPdo()->exec('ALTER TABLE bandwidth_spans ADD CONSTRAINT
			bandwidth_span_period UNIQUE (site_id, begindate)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
