<?php declare(strict_types=1);
	/**
	 * Hook facility (ActivateDomain)
	 * Called AFTER a domain is activated from suspension
	 * Parameters:
	 *   - string site
	 *
	 * This file, if present in config/custom/hooks/ named either
	 * activateDomain.sh or activateDomain.php will be called AFTER
	 * the account has been activated
	 */

	$n = 0;
	do {
		$path = dirname(__FILE__, ++$n);
	} while (!file_exists("$path/lib/config.php"));
	define('INCLUDE_PATH', realpath($path));
	include(INCLUDE_PATH . '/lib/CLI/cmd.php');

	$args = cli\parse();
	$c = cli\cmd(null, $args[0]);
	if (!is_object($c)) {
		fatal("Failed to create instance of `%s'", $args[0]);
	}

	/**
	 * Do whatever
	 */
	$ctx = \Auth::profile();
	echo "Hello as ", $c->common_whoami(), " on ", $ctx->domain, " (", $ctx->site, ")\n";
	echo "The domain has been activated. Its active state is:\n";
	// show all features enabled
	var_dump($ctx->getAccount()->active, "Disabled? " . \Opcenter\Account\State::disabled($ctx->site));

	/**
	 * The following code sample behaves similar to Service\Validators classes
	 * $s = \Opcenter\SiteConfiguration::import($ctx);
	 * $s->getSiteFunctionInterceptor();
	 */


	// post hook formatting
	$buffer = Error_Reporter::get_buffer();
	dlog("Suspend hook %s (%x) %s",
		$ctx->site,
		Error_Reporter::error_type(Error_Reporter::get_severity()),
		Error_Reporter::is_error() ? 'Failed' : 'Succeeded'
	);
	\cli\dump_buffer($buffer);

	exit (Error_Reporter::is_error());